(add-to-list 'load-path "./include/compose-publish/")
(require 'compose-publish)

(setq org-html-link-home "https://solverstack.gitlabpages.inria.fr/maphys/maphyspp/")
(setq org-html-link-up "../index.html")

(setq org-publish-project-alist
      (list
       (list "site-org"
             :base-directory "."
             :base-extension "org"
             :recursive nil
             :exclude "public"
             :publishing-function '(org-latex-publish-to-pdf-verbose-on-error org-html-publish-to-html)
             :publishing-directory "./public"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>")
       (list "site-static"
             :base-directory "."
             :base-extension "png\\|csv\\|txt\\|pdf\\|svg\\|yml\\|el\\|setup"
             :recursive t
             :exclude "public"
             :publishing-function '(org-publish-attachment)
             :publishing-directory "./public/")
       (list "slides"
             :base-directory "./slides"
             :base-extension "org"
             :publishing-function '(org-beamer-publish-to-pdf-verbose-on-error org-re-reveal-publish-to-reveal org-re-reveal-publish-to-reveal-client)
             :publishing-directory "./public/slides")
       (list "site" :components '("site-org" "site-static"))))

(provide 'compose-bibliography)
