#+TITLE: org-cite basic example
#+AUTHOR: =concace= team
#+SETUPFILE: ./include/compose-publish/theme-readtheorginria.setup

* Hello-world org-file

We can do an =org-cite= citation [cite:@OrgCitations] with this self-contained
set up.

#+bibliography: minibib.bib
#+cite_export: csl

The cite export engine can be set on a file basis (as here: ~#+cite_export:
csl~, not sure how to make it conditional in this case?) or globally (in which
case it can be furthermore handled conditionally depending on the back-end as
follows):
#+begin_src emacs-lisp :eval no
  (setq org-cite-export-processors '((latex biblatex)
                                     (t basic))))
#+end_src

* Bibliography

#+print_bibliography:

* Corresponding minimal setup

** Set up

#+BEGIN_SRC emacs-lisp :eval no
  (require 'oc)
  (require 'oc-biblatex) ;; for latex/pdf
  (require 'citeproc) ;; for html
  (require 'oc-csl) ;; for html
  (setq org-latex-pdf-process (list "latexmk --shell-escape -f -pdf %f"))
#+END_SRC

** batch export example

#+BEGIN_SRC bash :eval no
guix shell --pure emacs emacs-org emacs-org-ref emacs-citeproc-el -- emacs -q -nw --batch example-org-cite-basic.org --eval '(setq org-confirm-babel-evaluate nil)' -f org-babel-execute-buffer -f org-html-export-to-html --kill
#+END_SRC

** References

The [[https://orgmode.org/manual/Citation-handling.html][org-cite]] package has been a native citation syntax in =org-mode= [[https://list.orgmode.org/87o8bcnfk9.fsf@nicolasgoaziou.fr/][since]] 2021.
It may also be worth reading about [[https://kristofferbalintona.me/posts/202206141852/][combining]] =org=cite= and [[https://github.com/emacs-citar/citar][citar]].


